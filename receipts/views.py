from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def home_view(request):
    # obtain logged in user
    user = request.user
    receipts = Receipt.objects.all().filter(purchaser=user)
    context = {
        "receipt_list": receipts
    }
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()

            return redirect("home")
    else:
        form = ReceiptForm()

        context = {
            "create_receipt_form": form,
        }
        return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category": category
    }
    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account": account
    }
    return render(request, "receipts/accounts.html", context)

# @login_required
# def category_list(request):
#     categories = ExpenseCategory.objects.filter(owner=request.user)
#     context = {
#         'categories': categories
#     }

#     return render(request, 'receipts/category_list.html', context)


# @login_required
# def account_list(request):
#     accounts = Account.objects.filter(owner=request.user)
#     context = {
#         'accounts': accounts
#     }

#     return render(request, 'receipts/accounts.html', context)

@login_required
def create_category(request):
    if request.method == "POST":
        create_category_form = ExpenseCategoryForm(request.POST)
        if create_category_form.is_valid():
            category = create_category_form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        create_category_form = ExpenseCategoryForm()

    context = {
        "create_category_form": create_category_form
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        create_account_form = AccountForm(request.POST)
        if create_account_form.is_valid():
            account = create_account_form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        create_account_form = AccountForm()

    context = {
        "create_account_form": create_account_form
    }
    return render(request, "receipts/create_account.html", context)
